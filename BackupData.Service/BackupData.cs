﻿using Backup.Bussiness;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace BackupData.Service
{
    public partial class BackupData : ServiceBase
    {

        BackupBussiness backupBussiness = new BackupBussiness(); 
        public BackupData()
        {
            InitializeComponent();

#if DEBUG
            //Chỉ chạy dưới dang DEBUG
            backupBussiness = new BackupBussiness();
            backupBussiness.Start();
#endif
        }


        protected override void OnStart(string[] args)
        {
            if (!backupBussiness.Start())
                this.Stop();
        }

        protected override void OnStop()
        {
            if (backupBussiness.Started)
                backupBussiness.Stop();
        }
    }
}
