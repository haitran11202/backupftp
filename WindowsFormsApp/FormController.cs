﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

namespace WindowsFormsApp
{
    public partial class FormController : Form
    {
        private ServiceController _serviceController;

        private string BackupServiceName;
        public FormController()
        {
            InitializeComponent();

            this.BackupServiceName = "BackupService";
            _serviceController = new ServiceController();


        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        

        private void btnStop_Click(object sender, EventArgs e)
        {

        }

        private void btnStart_Click(object sender, EventArgs e)
        {

            

            _serviceController = new ServiceController(BackupServiceName);

            
                
            
            if (_serviceController.Status == ServiceControllerStatus.Stopped)
            {
                _serviceController.Start();
             
            }
            else if (_serviceController.Status == ServiceControllerStatus.Running)
            {
                _serviceController.Stop();
            }
        }
    }
}
