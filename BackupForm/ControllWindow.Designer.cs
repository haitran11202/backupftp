﻿namespace BackupForm
{
    partial class ControllWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            btnStartStop = new Button();
            btnConfig = new Button();
            label1 = new Label();
            label2 = new Label();
            label3 = new Label();
            lblStatus = new Label();
            SuspendLayout();
            // 
            // btnStartStop
            // 
            btnStartStop.AccessibleName = "BackupDataService";
            btnStartStop.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            btnStartStop.Location = new Point(659, 45);
            btnStartStop.Name = "btnStartStop";
            btnStartStop.Size = new Size(94, 29);
            btnStartStop.TabIndex = 1;
            btnStartStop.Text = "Start";
            btnStartStop.UseVisualStyleBackColor = true;
            btnStartStop.Click += btnStart_Click;
            // 
            // btnConfig
            // 
            btnConfig.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            btnConfig.Location = new Point(559, 45);
            btnConfig.Name = "btnConfig";
            btnConfig.Size = new Size(94, 29);
            btnConfig.TabIndex = 3;
            btnConfig.Text = "Config";
            btnConfig.UseVisualStyleBackColor = true;
            btnConfig.Click += btnConfig_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label1.ForeColor = SystemColors.Desktop;
            label1.Location = new Point(21, 45);
            label1.Name = "label1";
            label1.Size = new Size(283, 25);
            label1.TabIndex = 0;
            label1.Text = "Dịch vụ backup dữ liệu Cục trẻ em";
            label1.Click += label1_Click;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.FlatStyle = FlatStyle.Flat;
            label2.Font = new Font("Segoe UI Semibold", 9F, FontStyle.Bold, GraphicsUnit.Point);
            label2.ForeColor = Color.Blue;
            label2.Location = new Point(21, 9);
            label2.Name = "label2";
            label2.Size = new Size(87, 20);
            label2.TabIndex = 6;
            label2.Text = "Tên Service";
            label2.Click += label2_Click;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(402, 9);
            label3.Name = "label3";
            label3.Size = new Size(75, 20);
            label3.TabIndex = 7;
            label3.Text = "Trạng thái";
            label3.Click += label3_Click;
            // 
            // lblStatus
            // 
            lblStatus.AutoSize = true;
            lblStatus.Location = new Point(402, 49);
            lblStatus.Name = "lblStatus";
            lblStatus.Size = new Size(0, 20);
            lblStatus.TabIndex = 8;
            // 
            // ControllWindow
            // 
            AccessibleDescription = "";
            AccessibleName = "";
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(lblStatus);
            Controls.Add(label3);
            Controls.Add(btnStartStop);
            Controls.Add(btnConfig);
            Controls.Add(label2);
            Controls.Add(label1);
            MdiChildrenMinimizedAnchorBottom = false;
            Name = "ControllWindow";
            SizeGripStyle = SizeGripStyle.Show;
            Text = "Bảng điều khiển service";
            Load += ControllWindow_Load;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private Button btnStartStop;
        private Button btnConfig;
        private Label label1;
        private Label label2;
        private Label label3;
        private Label lblStatus;
    }
}