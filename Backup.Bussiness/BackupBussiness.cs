﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Backup.Bussiness
{
    public class BackupBussiness
    {
        public event Action<string> StatusUpdated;
        private string SourceFolderPath;
        private string ServerAddress;
        private string Username;
        private string Password;
        private int numberFileInServer;
        private Boolean runDequeueCleanData = true;
        private Thread threadBackupData;
        public Boolean Started { get; private set; }



       

        public BackupBussiness()
        {
            Initialize();
        }

        private void Initialize()
        {
            try
            {

                string xmlFilePath = "c:\\Users\\haith\\Desktop\\backup.xml";

                ServerInfor loadedUserData = LoadDataFromXml(xmlFilePath);

                // Sử dụng dữ liệu tải từ tệp XML
                Username = loadedUserData.Username;
                Password = loadedUserData.Password;
                numberFileInServer = loadedUserData.NumberFileBackup;
                ServerAddress = "ftp://" + loadedUserData.ServerAddress;
                SourceFolderPath = loadedUserData.FolderBackup;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private ServerInfor LoadDataFromXml(string filePath)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ServerInfor));
            using (StreamReader reader = new StreamReader(filePath))
            {
                return (ServerInfor)serializer.Deserialize(reader);
            }
        }


        public Boolean Start()
        {

            try
            {
                threadBackupData = new Thread(CompressFolder);
                threadBackupData.Start();
                Console.WriteLine("OK - CleanDataBusiness");
                Started = true;
                return Started;
            }
            catch (Exception)
            {

                this.Stop();
                return false;
            }


        }

        public Boolean Stop()
        {
            try
            {
                if (runDequeueCleanData)
                {
                    runDequeueCleanData = false;
                    threadBackupData.Abort();
                }
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        private void BackupDataThread()
        {
            while (runDequeueCleanData)
            {
                try
                {
                    CompressFolder();
                }
                catch (Exception ex)
                {

                    throw;
                }
                Thread.Sleep(3000);
            }
        }
        private void CompressFolder()
        {

            while (runDequeueCleanData)
            {

                StatusUpdated?.Invoke("Working thread....?");
                try
                {
                    string[] bakFiles = Directory.GetFiles(SourceFolderPath, "*.bak");

                    foreach (string bakFile in bakFiles)
                    {
                        string compressedFilePath = Path.ChangeExtension(bakFile, ".zip");

                        // Create a new ZIP file for the current .bak file
                        using (FileStream zipToCreate = new FileStream(compressedFilePath, FileMode.Create))
                        {
                            using (ZipArchive archive = new ZipArchive(zipToCreate, ZipArchiveMode.Create))
                            {
                                string entryName = Path.GetFileName(bakFile);
                                archive.CreateEntryFromFile(bakFile, entryName);
                            }
                        }
                        File.Delete(bakFile);
                        // Upload the compressed file to the server via FTP
                    }
                    var existingFiles = GetExistingFiles1(ServerAddress, Username, Password);
                    existingFiles.Sort((x, y) => y.CreationTime.CompareTo(x.CreationTime));
                    while (existingFiles.Count > numberFileInServer)
                    {
                        var oldestFile = existingFiles.Last();
                        DeleteFileFromServer(ServerAddress, oldestFile.Name, Username, Password);
                        existingFiles.Remove(oldestFile);
                    }
                    using (var client = new WebClient())
                    {
                        client.Credentials = new NetworkCredential(Username, Password);
                        string[] zipFilePaths = Directory.GetFiles(SourceFolderPath, "*.zip");

                        foreach (string zipFilePath in zipFilePaths)
                        {
                            string remoteFilePath = Path.GetFileName(zipFilePath);
                            using (var zipFileStream = File.OpenRead(zipFilePath))
                            {

                                //var request = (FtpWebRequest)WebRequest.Create(ServerAddress + destinationFilePath);

                                var request = (FtpWebRequest)WebRequest.Create(ServerAddress + "/" + remoteFilePath);

                                request.Method = WebRequestMethods.Ftp.UploadFile;
                                request.Credentials = new NetworkCredential(Username, Password);

                                using (var requestStream = request.GetRequestStream())
                                {
                                    zipFileStream.CopyTo(requestStream);
                                }

                                using (var response = (FtpWebResponse)request.GetResponse())
                                {
                                    Console.WriteLine($"Upload Complete for {remoteFilePath}, status {response.StatusDescription}");
                                }
                            }
                            File.Delete(zipFilePath);
                        }
                    }




                }
                catch (Exception ex)
                {
                    Console.WriteLine($"An error occurred: {ex.Message}");
                }

                Thread.Sleep(2000);

            }

            StatusUpdated?.Invoke("Stopped thread....?");

        }


        static List<FtpFileInfo> GetExistingFiles1(string ServerAddress, string username, string Password)
        {
            List<FtpFileInfo> existingFiles = new List<FtpFileInfo>();

            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(ServerAddress));
                request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                request.Credentials = new NetworkCredential(username, Password);

                using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        string line = reader.ReadLine();
                        while (!string.IsNullOrEmpty(line))
                        {
                            var fileInfo = new FtpFileInfo(line);
                            existingFiles.Add(fileInfo);
                            line = reader.ReadLine();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred while getting existing files: {ex.Message}");
            }

            return existingFiles;
        }

        static void DeleteFileFromServer(string ServerAddress, string fileName, string username, string Password)
        {
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri($"{ServerAddress}/{fileName}"));
                request.Method = WebRequestMethods.Ftp.DeleteFile;
                request.Credentials = new NetworkCredential(username, Password);

                using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
                {
                    Console.WriteLine($"File {fileName} deleted, status {response.StatusDescription}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred while deleting file {fileName}: {ex.Message}");
            }
        }

        class FtpFileInfo
        {
            public string Name { get; private set; }
            public DateTime CreationTime { get; private set; }

            public FtpFileInfo(string line)
            {
                string[] tokens = line.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (tokens.Length >= 4)
                {

                    DateTime creationTime = ParseDateTime(tokens[5], tokens[6], tokens[7]);

                    //Name = tokens[tokens.Length - 1];
                    string[] nameTokens = new string[tokens.Length - 8];
                    Array.Copy(tokens, 8, nameTokens, 0, tokens.Length - 8);
                    Name = string.Join(" ", nameTokens);
                    //DateTime.TryParse($"{tokens[5]} {tokens[6]} {tokens[7]}", out DateTime result);
                    CreationTime = creationTime;
                }
            }
            private string ExtractFileNameFromLine(string line)
            {
                int startIndex = line.IndexOf(' ');
                int endIndex = line.LastIndexOf(".zip");

                if (startIndex >= 0 && endIndex >= 0 && endIndex > startIndex)
                {
                    return line.Substring(startIndex + 1, endIndex - startIndex + 4);
                }

                return string.Empty;
            }

            static DateTime ParseDateTime(string monthAbbreviation, string day, string time)
            {
                string formattedDateTime = $"{monthAbbreviation} {day} {time}";
                return DateTime.ParseExact(formattedDateTime, "MMM d HH:mm", CultureInfo.InvariantCulture);
            }
        }
    }
}
