﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
namespace Backup
{
    [Serializable]
    public class ServerInfor
    {

        public string ServerAddress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public string FolderBackup { get; set; }
        public int NumberFileBackup { get; set; }
    }
}
