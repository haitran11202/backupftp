﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindowsService;

namespace WindowsService
{
    public partial class BackupService : ServiceBase
    {
        BackupServices backupServices = new WindowsService.BackupServices();

        public BackupService()
        {
            InitializeComponent();
#if DEBUG
            //Chỉ chạy dưới dang DEBUG
            backupServices = new BackupServices();
            backupServices.Start();
#endif

        }

        protected override void OnStart(string[] args)
        {
            if (!backupServices.Start())
                this.Stop();
        }

     
        protected override void OnStop()
        {
            if (backupServices.Started)
                backupServices.Stop();
        }

        
    }
}
