
namespace BackupForm
{
    public class ServerInfo
    {
        public string ServerAddress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }

}