﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BackupData.Models.ConfigModel
{
   
    [Serializable]
    public class ConfigData
    {
        public string ServerAddress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int MaxFilesOnServer { get; set; }
    }

}
