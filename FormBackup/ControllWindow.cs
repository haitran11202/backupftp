﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsService.BackupServices;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;

namespace BackupForm
{
    public partial class ControllWindow : Form
    {
        public ServerInfor serverInfor;
        private BackupServices backupServices;
        private Thread workerThread;

        public bool isRunning;
        public ControllWindow()
        {

            serverInfor = new ServerInfor();
            serverInfor.ServerAddress = "192.168.100.104";
            serverInfor.Username = "backup";
            serverInfor.Password = "123456";
            serverInfor.NumberFileBackup = 5;
            serverInfor.FolderBackup = "D://backup";
            isRunning = false;

            backupServices = new BackupServices(serverInfor.FolderBackup, serverInfor.ServerAddress, serverInfor.Username, serverInfor.Password, serverInfor.NumberFileBackup);
            InitializeComponent();
        }



        //private void label2_Click(object sender, EventArgs e)
        //{

        //}

        private void btnConfig_Click(object sender, EventArgs e)
        {


            using (ConfigWindow configForm = new ConfigWindow())
            {
                if (configForm.ShowDialog() == DialogResult.OK)
                {


                    serverInfor = configForm.ServerInfor;

                }
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            //Luồng chạy backup

            backupServices.Start();
            btnStart.Enabled = false;
            btnStop.Enabled = true;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            backupServices.Stop();
            btnStart.Enabled = true;
            btnStop.Enabled = false;
        }
    }
}
