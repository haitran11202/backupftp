﻿namespace BackupForm
{
    partial class ConfigWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            panel1 = new Panel();
            button1 = new Button();
            txtSelectedFolder = new TextBox();
            btnLuu = new Button();
            txtThoiGianThucHien = new TextBox();
            label6 = new Label();
            txtSoFileLuuTru = new TextBox();
            label5 = new Label();
            txtMatKhau = new TextBox();
            label4 = new Label();
            txtTaiKhoan = new TextBox();
            label3 = new Label();
            txtDiaChiFTP = new TextBox();
            label2 = new Label();
            label1 = new Label();
            panel1.SuspendLayout();
            SuspendLayout();
            // 
            // panel1
            // 
            panel1.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            panel1.Controls.Add(button1);
            panel1.Controls.Add(txtSelectedFolder);
            panel1.Controls.Add(btnLuu);
            panel1.Controls.Add(txtThoiGianThucHien);
            panel1.Controls.Add(label6);
            panel1.Controls.Add(txtSoFileLuuTru);
            panel1.Controls.Add(label5);
            panel1.Controls.Add(txtMatKhau);
            panel1.Controls.Add(label4);
            panel1.Controls.Add(txtTaiKhoan);
            panel1.Controls.Add(label3);
            panel1.Controls.Add(txtDiaChiFTP);
            panel1.Controls.Add(label2);
            panel1.Controls.Add(label1);
            panel1.Location = new Point(4, 6);
            panel1.Name = "panel1";
            panel1.Size = new Size(784, 432);
            panel1.TabIndex = 0;
            panel1.Paint += panel1_Paint;
            // 
            // button1
            // 
            button1.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            button1.Location = new Point(611, 52);
            button1.Name = "button1";
            button1.Size = new Size(94, 29);
            button1.TabIndex = 14;
            button1.Text = "Chọn";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // txtSelectedFolder
            // 
            txtSelectedFolder.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            txtSelectedFolder.Location = new Point(299, 52);
            txtSelectedFolder.Name = "txtSelectedFolder";
            txtSelectedFolder.Size = new Size(299, 27);
            txtSelectedFolder.TabIndex = 13;
            txtSelectedFolder.TextChanged += txtSelectedFolder_TextChanged;
            // 
            // btnLuu
            // 
            btnLuu.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            btnLuu.Location = new Point(611, 380);
            btnLuu.Name = "btnLuu";
            btnLuu.Size = new Size(94, 29);
            btnLuu.TabIndex = 12;
            btnLuu.Text = "Lưu";
            btnLuu.UseVisualStyleBackColor = true;
            btnLuu.Click += btnLuu_Click;
            // 
            // txtThoiGianThucHien
            // 
            txtThoiGianThucHien.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            txtThoiGianThucHien.Location = new Point(299, 286);
            txtThoiGianThucHien.Name = "txtThoiGianThucHien";
            txtThoiGianThucHien.Size = new Size(406, 27);
            txtThoiGianThucHien.TabIndex = 11;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label6.Location = new Point(79, 285);
            label6.Name = "label6";
            label6.Size = new Size(163, 25);
            label6.TabIndex = 10;
            label6.Text = "Thời gian thực hiện";
            // 
            // txtSoFileLuuTru
            // 
            txtSoFileLuuTru.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            txtSoFileLuuTru.Location = new Point(299, 238);
            txtSoFileLuuTru.Name = "txtSoFileLuuTru";
            txtSoFileLuuTru.Size = new Size(250, 27);
            txtSoFileLuuTru.TabIndex = 9;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label5.Location = new Point(71, 237);
            label5.Name = "label5";
            label5.Size = new Size(171, 25);
            label5.TabIndex = 8;
            label5.Text = "Số lượng file lưu trữ";
            // 
            // txtMatKhau
            // 
            txtMatKhau.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            txtMatKhau.Location = new Point(299, 197);
            txtMatKhau.Name = "txtMatKhau";
            txtMatKhau.Size = new Size(406, 27);
            txtMatKhau.TabIndex = 7;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label4.Location = new Point(162, 196);
            label4.Name = "label4";
            label4.Size = new Size(86, 25);
            label4.TabIndex = 6;
            label4.Text = "Mật khẩu";
            // 
            // txtTaiKhoan
            // 
            txtTaiKhoan.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            txtTaiKhoan.Location = new Point(299, 152);
            txtTaiKhoan.Name = "txtTaiKhoan";
            txtTaiKhoan.Size = new Size(406, 27);
            txtTaiKhoan.TabIndex = 5;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label3.Location = new Point(162, 152);
            label3.Name = "label3";
            label3.Size = new Size(86, 25);
            label3.TabIndex = 4;
            label3.Text = "Tài khoản";
            // 
            // txtDiaChiFTP
            // 
            txtDiaChiFTP.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            txtDiaChiFTP.Location = new Point(299, 100);
            txtDiaChiFTP.Name = "txtDiaChiFTP";
            txtDiaChiFTP.Size = new Size(406, 27);
            txtDiaChiFTP.TabIndex = 3;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label2.Location = new Point(98, 99);
            label2.Name = "label2";
            label2.Size = new Size(150, 25);
            label2.TabIndex = 2;
            label2.Text = "Địa chỉ server FTP";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label1.Location = new Point(104, 52);
            label1.Name = "label1";
            label1.Size = new Size(144, 25);
            label1.TabIndex = 0;
            label1.Text = "Thư mục backup";
            label1.Click += label1_Click;
            // 
            // ConfigWindow
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(panel1);
            Name = "ConfigWindow";
            Text = "ConfigWindow";
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private Panel panel1;
        private Label label1;
        private TextBox txtThoiGianThucHien;
        private Label label6;
        private TextBox txtSoFileLuuTru;
        private Label label5;
        private TextBox txtMatKhau;
        private Label label4;
        private TextBox txtTaiKhoan;
        private Label label3;
        private TextBox txtDiaChiFTP;
        private Label label2;
        private Button btnLuu;
        private TextBox txtSelectedFolder;
        private Button button1;
    }
}