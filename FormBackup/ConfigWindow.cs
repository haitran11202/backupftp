﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace BackupForm
{
    public partial class ConfigWindow : Form
    {   
        public ServerInfor ServerInfor { get; set; }


       
        public ConfigWindow(ServerInfor serverInfor)
        {

            InitializeComponent();
            ServerInfor = serverInfor;
        }

        public ConfigWindow()
        {

           
            ServerInfor = new ServerInfor();
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog())
            {
                // Hiển thị hộp thoại chọn thư mục
                DialogResult result = folderBrowserDialog.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(folderBrowserDialog.SelectedPath))
                {
                    // Hiển thị đường dẫn thư mục đã chọn lên Form
                    txtSelectedFolder.Text = folderBrowserDialog.SelectedPath;
                }
            }
        }

        private void txtSelectedFolder_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            // Lấy thông tin từ các điều khiển trên form và gán vào ServerInfo

            

            ServerInfor.ServerAddress = txtDiaChiFTP.Text;
            ServerInfor.Username = txtTaiKhoan.Text;
            ServerInfor.Password = txtMatKhau.Text;
            ServerInfor.NumberFileBackup =  Int32.Parse(txtSoFileLuuTru.Text);
            ServerInfor.FolderBackup = txtSelectedFolder.Text;


            // Loại bỏ các ký tự xuống dòng ở cuối chuỗi
            string trimmedText = ServerInfor.ServerAddress.TrimEnd('\n', '\r');

            // Gán lại giá trị đã được loại bỏ xuống dòng vào Text Area
            ServerInfor.ServerAddress = trimmedText;
            DialogResult = DialogResult.OK;
            Close();
        }
    }
}
