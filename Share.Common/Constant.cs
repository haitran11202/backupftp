﻿

namespace Share.Common
{
    

    public enum ServiceStatus
    {
        NotInstalled = 0,
        Stopped = 1,
        Running = 2
    }
}
